<div align="center"> <img alt="CoderQ" width="200" height="200" src="https://cdn.jsdelivr.net/gh/PlayGuitar-CoderQ-Sub/img_bed@master/uPic/compasses.png"><br> <br>

[![license](https://img.shields.io/badge/React-17.0.0-blue?logo=React)](LICENSE)
[![license](https://img.shields.io/badge/Recoil-0.4.1-196cdc?logo=Recoil)](LICENSE)
[![license](https://img.shields.io/badge/Antd-4.16.3-9cf?logo=AntDesign)](LICENSE)
[![license](https://img.shields.io/badge/Vite-2.6.4-blueviolet?logo=Vite)](LICENSE)
[![license](https://img.shields.io/badge/TypeScript-4.3.2-blue?logo=TypeScript)](LICENSE)

<br />

<h1>圆规计划后台项目管理系统</h1>
</div>

## ⚡️ 项目亮点

1.  🦹‍♂️ 细致的注释，方便开发人员进行学习阅读
2.  🕹 完整的项目管理流程，保证项目的严谨开发
3.  🌟 vite 的加持，让开发速度提速
4.  👑 采用新颖的前端技术栈服务项目

## 🕹 项目启动

```shell
# 安装依赖
yarn isntall

# 启动项目
yarn start


# 项目打包
yarn build
```

## 🟢 浏览器支持

推荐本地开发的 `Chrome 80 +` 浏览器支持现代浏览器，而不是 IE

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                                                                             not support                                                                                              |                                                                                            last 2 versions                                                                                             |                                                                                                  last 2 versions                                                                                                  |                                                                                                last 2 versions                                                                                                |                                                                                                last 2 versions                                                                                                |

## MIT 协议

[MIT © CoderQ-2020](./LICENSE)

## 提交测试
