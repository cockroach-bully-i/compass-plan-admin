import type { FC } from 'react';

import React from 'react';

import './index.css';

import Logo from '@/assets/compasses.png';

const Demo: FC = () => {
  return (
    <div className="demo">
      <img src={Logo} alt="" />
      <h1>圆规计划</h1>
    </div>
  );
};

export default Demo;
