import React from 'react';

import Demo from '@/views/Demo';

const App = () => {
  return (
    <div>
      <Demo />
    </div>
  );
};

export default App;
