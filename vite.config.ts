import type { UserConfig } from 'vite';

import { resolve } from 'path';
import react from '@vitejs/plugin-react'

function pathResolve(dir: string): string {
  return resolve(process.cwd(), '.', dir);
}

export default (): UserConfig => {
  return {
    plugins: [react()],
    resolve: {
      alias: [
        {
          find: '@',
          replacement: pathResolve('src') + '/',
        }
      ]
    }
  }
}
